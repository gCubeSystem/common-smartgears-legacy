This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

# Changelog for Common Smartgears

## [v3.1.6]

- Added Linux distribution version [#22933]

## [v3.1.5] - 2022-04-20

- Added roles to ExternalService Info on request handler verification

## [v3.1.4] - 2022-03-29

- fixes issue [#23075]

## [v3.1.3] - 2022-03-21

- fixed bug on policies


## [v3.1.2] - 2022-01-19

- enabled policy check on smartgears
- container configuration for test added

## [v3.1.1] - 2021-09-29

- minimal privilege granted also on empty resource_access in JWT token

## [v3.1.0] - 2021-05-14

- use of AccessTokenProvider
- use gcube-jackson instead of minimal-json for access token parsing  [#21097]


## [v3.0.2] - 2020-03-01

- check if response  is already committed on error


## [v3.0.1] - 2020-11-18

- new Uma Token integration

## [v3.0.0] - 2020-10-20

- Switched container JSON management to gcube-jackson [#19283]

## [v.2.2.0] - 2020-01-23

- Multiple token are generated in the same call in place of one per call


## [v.2.1.9] - 2019-11-08

- Project adapted to be build with Jenkins


## [v.2.1.8]  - 2019-05-27

- Support oauth2 protocol accepting token in the auhtorization header field


## [v.2.1.7]  - 2019-02-26

- Added Proxy Address to Application Configuration
- Added protocol to Container Configuration (http by default)
- Changed the logs in accounting handler to log error or success and eventually error code


## [v.2.1.5] - 2017-09-19

- Added ThreadLocal InnerMethodName to set method name from application


## [v.2.1.4] - 2017-07-25

- Validation handler for application split in 2 different handlers: - ContextRetriever that set Token and Scope - RequestValidation that does all the required checks


## [v.2.1.3] - 2017-06-06

- Added gcube bom dependency
- Search for handlers in the root classpath


## [v.2.1.2]  - 2017-05-02

- Modified the Authorization filter to accept also children scope when authorizeChildrenContext is enabled on ContianerConfiguration
- Shutdown of Accounting thread added


## [v.2.1.1] - 2017-03-16

- Minor issue on filter exclusion fixed


## [v.2.0.1] - 2016-12-15

- Proxy configuration added
- Solved a bug in events registration for ProfileManager
- Added a scheduler for period update of GCoreEnpoints
- Exclude modified to support exclude for sub-group of handlers


## [v.2.0.0]  - 2016-11-07

- Integration with Authorization 2.0


## [v.1.2.7]  - 2016-05-18

- Removed commons-io dependency [#2355]

## [v.1.2.6]  - 2016-04-08

- Added missing class for service loader of org.gcube.smartgears.handlers.container.ContainerHandler [#2474]
- Added flush of accounting data [#1353]


## [v.1.2.5]  - 2016-02-08

- Enhanced accounting version


## [v.1.2.4]  - 2015-12-09

- Transparent accounting added on service calls


## [v.1.2.3] - 2015-07-27

- Authorization token control added
- Added support to HTTP Basic authorization


## [v.1.2.2] - 2015-04-27

- Fixed available space information on ghn profile


## [v.1.2.1] - 2014-02-13

- Scopes can be removed from container
- Node profile set to static
- Internal adjustments for move to Java 7
- Wildcard allowed in exclude directives
- Domain corrected derived in gHN profile
- Cleaner shutdown
- Further improvement in shutdown handling


## [v.1.0.0] - 2013-10-24

- First Release

