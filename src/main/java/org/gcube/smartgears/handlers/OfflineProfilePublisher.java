package org.gcube.smartgears.handlers;

import java.util.Collection;

public class OfflineProfilePublisher implements ProfilePublisher {

	@Override
	public void addTo(Collection<String> tokens) {
	}

	@Override
	public void addToAll() {
	}

	@Override
	public void update() {
	}

	@Override
	public void removeFrom(Collection<String> tokens) {
	}

	
}
