package org.gcube.smartgears.handlers;

import java.util.Collection;

public interface ProfilePublisher {

	/**
	 * Adds for the first time the current resource profile of the application in one or more scopes.
	 * @param scopes the scopes
	 */
	void addTo(Collection<String> tokens);

	void addToAll();

	void update();

	/**
	 * Removes the application from one or more scopes.
	 * @param scopes the scopes
	 */
	void removeFrom(Collection<String> tokens);

}