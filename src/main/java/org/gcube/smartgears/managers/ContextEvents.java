package org.gcube.smartgears.managers;

public class ContextEvents {

	public static final String ADD_TOKEN_TO_CONTAINER ="AddTokenToContainer";

	public static final String ADD_TOKEN_TO_APPLICATION ="AddTokenToApplication";

	public static final String REMOVE_TOKEN_FROM_CONTAINER ="RemoveTokenFromContainer";

	public static final String REMOVE_TOKEN_FROM_APPLICATION ="RemoveTokenFromApplication";

}
