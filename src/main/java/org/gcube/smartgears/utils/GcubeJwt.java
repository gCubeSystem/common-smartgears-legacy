package org.gcube.smartgears.utils;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.gcube.com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.gcube.com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class GcubeJwt {

	protected final static List<String> MINIMAL_ROLES = Arrays.asList("Member");

	@JsonProperty("aud")
	private String context;
		
	@JsonProperty("resource_access")
	private Map<String, Roles> contextAccess = new HashMap<>();
	
	@JsonProperty("preferred_username")
	private String username;
	
	@JsonProperty("given_name")
	private String firstName;
	
	@JsonProperty("family_name")
	private String lastName;
	
	@JsonProperty("clientId")
	private String clientId;
	
	@JsonProperty("email")
	private String email;

	public List<String> getRoles(){
		return contextAccess.get(this.context) == null ? MINIMAL_ROLES : contextAccess.get(this.context).roles;
	}
	
	public String getContext() {
		 try {
			return  URLDecoder.decode(context, StandardCharsets.UTF_8.toString());
		 }catch (UnsupportedEncodingException e) {
			return context;
		}
	}
		
	public String getUsername() {
		return username;
	}

	public boolean isExternalService() {
		 return clientId != null;
	}
	
	public String getFirstName() {
		return firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public String getEmail() {
		return email;
	}

	@Override
	public String toString() {
		return "GcubeJwt [context=" + getContext() + ", roles=" + getRoles() + ", username=" + username
				+ ", firstName=" + firstName + ", lastName=" + lastName + ", email=" + email + "]";
	}

	public static class Roles {
				
		@JsonProperty("roles")
		List<String> roles = new ArrayList<>();
	
	}
	
}
